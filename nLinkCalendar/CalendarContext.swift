//
//  CalendarContext.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/7/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit
import RxSwift
import EventKit

class CalendarContext {
  
  static let current = CalendarContext()

  var currentDay = Variable(Date.today)
  
  weak var delegate: CalendarDeledate?
  
  weak var controller: UIViewController?
  
  private var eventStore = EKEventStore()
  
  private var currentStart : Date?
  private var eventsByDayCache = [String:[CalendarEvent]]()
  
  lazy var nativeCalendars : [CalendarGroup] = {
    var cals = [CalendarGroup]()
    for calendar in self.eventStore.calendars(for: .event) {
      let group = CalendarGroup(name: calendar.title, color: UIColor(cgColor: calendar.cgColor))
      cals.append(group)      
    }
    return cals
  }()
  
  var selectedGroups = Variable(Set<CalendarGroup>())
  
  func dayEvents(for day: Date, callback: @escaping ([CalendarEvent]) -> Void) {
    let key = cacheKey(for: day)
    if let events = eventsByDayCache[key] {
      print("Cache hit for : \(day)")
      return callback(events)
    }
    print("Loading events for: \(day)")
    let delegateEvents = delegate?.events(from: day.startOfDay, to: day.endOfDay ?? day) ?? []
    var calEvents = [CalendarEvent]()
    let respond = { (events: [CalendarEvent]) in
      self.eventsByDayCache[key] = events
      callback(events)
    }
    eventStore.requestAccess(to: EKEntityType.event, completion: {
      (accessGranted: Bool, error: Error?) in
      
      if accessGranted == true {
        let ekEvents = self.eventStore.events(matching: self.eventStore.predicateForEvents(withStart: day.startOfDay, end: day.endOfDay ?? day, calendars: nil))
        
        
        for ekEvent in ekEvents {
          let group = CalendarGroup(name: ekEvent.calendar.title, color: UIColor(cgColor: ekEvent.calendar.cgColor))
          calEvents.append(CalendarEvent(title: ekEvent.title, start: ekEvent.startDate, end: ekEvent.endDate, allDay: ekEvent.isAllDay, additionalLines: [ekEvent.location ?? ""], group: group, handback: ekEvent))
        }
        
        let results = delegateEvents + calEvents
        let filtered = results.filter({
          return self.selectedGroups.value.contains($0.group)
        })
        DispatchQueue.main.async {
          respond(filtered)
        }
      } else {
        DispatchQueue.main.async {
          respond(delegateEvents)
        }
      }
    })
  }
  
  func currentDayEvent(callback: @escaping ([CalendarEvent]) -> Void) {
    dayEvents(for: currentDay.value, callback: callback)
  }
  
  func detailController(for event: CalendarEvent) -> UIViewController? {
    return delegate?.detailController(for: event)
  }
  
  func displayDetailModal(for event: CalendarEvent) {
    guard let detailController = detailController(for: event) else { return }
    controller?.present(detailController, animated: true, completion: nil)
  }
  
  private func cacheByDay(events: [CalendarEvent]) {
    for event in events {
      let key = cacheKey(for: event.start)
      if eventsByDayCache[key] != nil {
        eventsByDayCache[key]?.append(event)
      } else {
        eventsByDayCache[key] = [event]
      }
    }
  }
  
  func change(day: Date) {
    let rangeStart = day.startOfWeek.subtract(1.weeks)
    let rangeEnd = day.endOfWeek.add(1.weeks)
    if rangeStart != currentStart {
      print("Load em up")
      var allEvents = [CalendarEvent]()
      let delegateEvents = delegate?.events(from: rangeStart, to: rangeEnd) ?? []
      allEvents.append(contentsOf: delegateEvents)
      let process = { (events: [CalendarEvent]) in
        let filtered = allEvents.filter({
          return self.selectedGroups.value.contains($0.group)
        })
        self.cacheByDay(events: filtered)
      }
      eventStore.requestAccess(to: EKEntityType.event, completion: {
        (accessGranted: Bool, error: Error?) in
        
        if accessGranted == true {
          let ekEvents = self.eventStore.events(matching: self.eventStore.predicateForEvents(withStart: rangeStart, end: rangeEnd, calendars: nil))
          for ekEvent in ekEvents {
            let group = CalendarGroup(name: ekEvent.calendar.title, color: UIColor(cgColor: ekEvent.calendar.cgColor))
            allEvents.append(CalendarEvent(title: ekEvent.title, start: ekEvent.startDate, end: ekEvent.endDate, allDay: ekEvent.isAllDay, additionalLines: [ekEvent.location ?? ""], group: group, handback: ekEvent))
          }
          process(allEvents)
        } else {
          process(allEvents)
        }
      })
      currentStart = rangeStart
      currentDay.value = day
    } else {
      currentDay.value = day
    }
    
  }
  
  func reload() {
    eventsByDayCache.removeAll()
  }
  
  private func cacheKey(for day: Date) -> String {
    // TODO(JEB): Memoize formatter
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return formatter.string(from: day)
  }
  
}
