//
//  CalendarDatasource.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/7/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

protocol CalendarDeledate : class {

  var groups : [CalendarGroup] { get }
  
  func events(from: Date, to: Date) -> [CalendarEvent]
  
  func selected(event: CalendarEvent)
  
  func detailController(for: CalendarEvent) -> UIViewController?
  
  func cell(for: CalendarEvent) -> UIView?
}

extension CalendarDeledate {
  
  func selected(event: CalendarEvent) {
  }
  
  func detailController(for event: CalendarEvent) -> UIViewController? {
    return nil
  }
  
  func cell(for event: CalendarEvent) -> UIView? {
      return EventCell(event: event)
  }

}
