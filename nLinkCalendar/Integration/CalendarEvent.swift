//
//  Event.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/7/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

struct CalendarEvent {

  let id : String
  
  let title : String
  
  let additionalLines : [String]
  
  let start: Date
  
  let end: Date
  
  let allDay: Bool
  
  let group: CalendarGroup

  let handback: Any?
  
  init(id: String = UUID().uuidString, title: String, start: Date, end: Date, allDay: Bool  = false, additionalLines: [String] = [], group: CalendarGroup, handback: Any? = nil) {
    self.id = id
    self.title = title
    self.start = start
    self.end = end
    self.allDay = allDay
    self.additionalLines = additionalLines
    self.group = group
    self.handback = handback
  }
}
