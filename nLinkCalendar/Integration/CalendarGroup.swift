//
//  CalendarGroup.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/12/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

struct CalendarGroup : Hashable {
  
  var hashValue: Int {
    return name.hashValue
  }
  
  let name: String
  
  let color: UIColor
  
  init(name: String, color: UIColor = UIColor.mdtBlue) {
    self.name = name
    self.color = color
  }

  static func ==(lhs: CalendarGroup, rhs: CalendarGroup) -> Bool {
    return lhs.name == rhs.name
  }
}
