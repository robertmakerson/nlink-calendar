//
//  ConstraintExtensions.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/5/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import Foundation
import UIKit

extension NSLayoutConstraint {
 
  func updateMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
    NSLayoutConstraint.deactivate([self])
    let newConstraint = NSLayoutConstraint(item: self.firstItem, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    newConstraint.priority = self.priority
    newConstraint.shouldBeArchived = self.shouldBeArchived
    newConstraint.identifier = self.identifier
    newConstraint.isActive = true
    NSLayoutConstraint.activate([newConstraint])
    return newConstraint
  }
}
