//
//  DateExtensions.swift
//  nLinkCalendar
//
//  Created by Julian Hays on 2/27/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import Foundation

fileprivate let DayComponents: [Calendar.Component] = [.year, .month, .day]

extension Calendar {
  static let main = Calendar(identifier: .gregorian)
}

extension Date {
  
  static var today: Date {
    return Date().startOfDay
  }
  
  var startOfDay: Date {
    return Calendar.main.startOfDay(for: self)
  }
  
  var endOfDay: Date? {
    var components = DateComponents()
    components.day = 1
    components.second = -1
    return Calendar.main.date(byAdding: components, to: self)
  }
 
  var startOfWeek: Date {
    return Calendar.main.date(from: Calendar.main.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) ?? Date()
  }
  
  public var endOfWeek: Date {
    return startOfWeek.add(6.days)
  }
  
  public var startOfMonth: Date {
    var components = Calendar.main.dateComponents(Set(DayComponents), from: self)
    components.day = 1
    return Calendar.main.date(from: components) ?? Date()
  }
  
  public var endOfMonth: Date {
    var components = Calendar.main.dateComponents(Set(DayComponents), from: self)
    guard let month = components.month else { return Date() }
    components.month = month + 1
    components.day = 0
    return Calendar.main.date(from: components) ?? Date()
  }
  
  public var minutesIntoDay : Int {
    let calendar = Calendar.main
    let hour = calendar.component(.hour, from: self)
    let minute = calendar.component(.minute, from: self)
    return hour * 60 + minute
  }
  

  
}

struct DateRange {
  let start: Date
  let end: Date
  
  public func overlaps(other: DateRange) -> Bool {
    
//    return
//      //The spans overlap if spanB's start is inside spanA
//      (spanB.start > spanA.start && spanB.start < spanA.en)
//        ||
//        //or if spanB's END is inside spanA
//        (spanB.end > spanA.start && spanB.end < spanA.end)
//        ||
//        //or if spanA's start is inside spanB
//        (spanA.start > spanB.start && spanA.start < spanB.end);
    
    return
      (other.start > start && other.start < end)
      ||
      (other.end > start && other.end < end)
      ||
      (start > other.start && start < other.end)
      || start == other.start    
  }
}
