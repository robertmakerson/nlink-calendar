//
//  FontExtensions.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/12/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

extension UIFont {

  static func regular(size: Int) -> UIFont {
    return UIFont(name: "Effra-Regular", size: CGFloat(size))!
  }
  
  static func light(size: Int) -> UIFont {
    return UIFont(name: "Effra-Light", size: CGFloat(size))!
  }
  
  static func bold(size: Int) -> UIFont {
    return UIFont(name: "Effra-Bold", size: CGFloat(size))!
  }
  
  static func medium(size: Int) -> UIFont {
    return UIFont(name: "Effra-Medium", size: CGFloat(size))!
  }


}
