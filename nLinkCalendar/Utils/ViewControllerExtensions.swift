//
//  ViewControllerExtensions.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/14/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

extension UIViewController {

  @IBAction public func close(sender: Any) {
    dismiss(animated: true, completion: nil)
  }

}
