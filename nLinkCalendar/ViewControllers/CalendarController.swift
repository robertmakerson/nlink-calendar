//
//  ViewController.swift
//  nLinkCalendar
//
//  Created by Julian Hays on 2/22/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

private let topHeightIphone = CGFloat(0)

class CalendarController: ControllerBase {

  @IBOutlet weak var ipadHeaderView: UIView?
  
  @IBOutlet weak var headerTitle: DateTitleControl?
  
  @IBOutlet weak var subviewSelector: UISegmentedControl?
  
  @IBOutlet weak var subviewContainerTop: NSLayoutConstraint?
  
  @IBOutlet weak var subviewContainer: UIView?
  
  var dayView = DayView()
  
  let weekView = WeekView()
  
  let scheduleView = ScheduleView()
  
  let agendaView = AgendaView()
  
  let monthPicker = MonthPicker()
  
  var context : CalendarContext {
    return CalendarContext.current
  }

  var groupPickerController : UIViewController?
  
  let mockGroups = [
    CalendarGroup(name: "Group 1", color: UIColor.mdtBlue),
    CalendarGroup(name: "Group 2", color: UIColor.mdtGreen),
    CalendarGroup(name: "Group 3", color: UIColor.mdtOrange),
    CalendarGroup(name: "Group 4", color: UIColor.mdtYellow),
    CalendarGroup(name: "Group 5", color: UIColor.mdtTurquoise),
    CalendarGroup(name: "Group 6", color: UIColor.mdtLightPurple)
  ]
  
  let mockEventData = [
    (
      "PS Follow-Up",
      "Piperis Interventional Pain Care",
      "Dr Griffith Evans",
      "Jane Doe"
    ),
    (
      "PS Trial – Evolve Workflow",
      "Kearney Regional Medical Center LLC",
      "Dr J Paul Meyer",
      "Kevin Thomas"
    ),
    (
    "PS Implant",
    "Kearney Pain Treatment Center",
    "Dr J Paul Meyer",
    "Bob Evans"
    ),
    (
    "PP Drug Refill & Reprogram",
    "VAMC 636 Omah",
    "Dr Wesley Prickett",
    "Tyler Smith"
    ),
    (
    "PS Sensor Activation",
    "Regions Hospital",
    "Dr Kim Haynes-Henson",
    "Joe Smith"
    ),
    (
      "Meet Bell Perkins",
      "Breakfast bar? Javier",
      "3235137701",
      ""
    ),
    ("Sean to Vancouver for AAPM with UNMC docs", "", "", ""),
    ("Dentist Sean", "", "", ""),
    ("PP phone and text troubleshooting", "", "", ""),
    ("Office email, expenses, forecast, inventory, pt follow-up", "", "", ""),
    ("Bring demo to Raiput", "", "", ""),
  ]
  
  let mockLunches = [
    ("Lunch", "", "", ""),
    ("Lunch essay", "14 Lincoln", "", ""),
  ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configUI()
    context.controller = self
    context.currentDay
      .asObservable()
      .subscribe(onNext: update)
      .disposed(by: disposeBag)
    
    // TODO(JEB): Remove mock stuff
    var  selectedGroups = Set<CalendarGroup>()
    for group in mockGroups {
      selectedGroups.insert(group)
    }
    for group in context.nativeCalendars {
      selectedGroups.insert(group)
    }
    context.selectedGroups.value = selectedGroups
    context.delegate = self
    configure()
    context.change(day: Date.today)
  }

  private func configUI() {
    let font = UIFont.regular(size: 15)
    subviewSelector?.setTitleTextAttributes([NSAttributedStringKey.font: font], for: .normal)
    subviewSelector?.rx.value
      .subscribe(onNext: changeView)
      .disposed(by: disposeBag)
    if UIDevice.current.userInterfaceIdiom == .pad {
      navigationController?.setNavigationBarHidden(true, animated: false)
      ipadHeaderView?.isHidden = false
      monthPicker.frame = CGRect(x: 0, y: ipadHeaderView?.bounds.height ?? 0, width: 300, height: 0)
    } else {
      guard let headerTitle = headerTitle else { return }
      navigationItem.leftBarButtonItem = UIBarButtonItem(customView: headerTitle)
      DispatchQueue.main.async {
        self.subviewContainerTop?.constant = topHeightIphone
        self.monthPicker.frame = CGRect(x: 0, y: 84, width: 300, height: 0)
        self.view.layoutIfNeeded()
      }
    }
    subviewContainer?.autoresizesSubviews = true
    dayView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    weekView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    view.addSubview(monthPicker)
    headerTitle?.onTap = tapDate
  }

  // MARK: View Delegation
  func configure() {
    dayView.configure()
    weekView.configure()
    agendaView.configure()
  }
  
  func update(day: Date) {   
  }
  
  func move(to date: Date) {
    context.change(day: date)
  }
  
  func changeView(subview: Int) {
    for view in subviewContainer?.subviews ?? [] {
      view.removeFromSuperview()
    }
    if subview == 0 {
      dayView.frame = subviewContainer?.bounds ?? .zero
      subviewContainer?.addSubview(dayView)
    } else if subview == 1 {
      weekView.frame = subviewContainer?.bounds ?? .zero
      subviewContainer?.addSubview(weekView)
    } else if subview == 2 {
      scheduleView.frame = subviewContainer?.bounds ?? .zero
      subviewContainer?.addSubview(scheduleView)
    } else if subview == 3 {
      agendaView.frame = subviewContainer?.bounds ?? .zero
      subviewContainer?.addSubview(agendaView)
    }
  }
  
  // MARK: UI Actions
  
  @IBAction func goToday() {
    move(to: Date())
  }
  
  @IBAction func tapDate(_ sender: Any) {
    monthPicker.toggle()
  }
  
  @IBAction func toggleAgenda(button: UIButton) {
    guard let subviewSelector = subviewSelector else {
      return
    }
    if subviewSelector.selectedSegmentIndex == 0 {
      changeView(subview: 3)
      subviewSelector.selectedSegmentIndex = 3
      button.backgroundColor = .mdtMediumBlue
      button.setTitleColor(.white, for: .normal)
    } else {
      subviewSelector.selectedSegmentIndex = 0
      changeView(subview: 0)
      button.backgroundColor = .clear
      button.setTitleColor(.mdtMediumBlue, for: .normal)
    }
  }
  
  @objc func dateChanged(_ sender: UIDatePicker) {
    move(to: sender.date)
  }
  
  @IBAction func toggleGroupPicker(button: UIButton) {        
    let controller = UIViewController()
    controller.title = "Views"
    controller.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(closeGroupPicker))
    let groupPicker = GroupPicker()
    groupPicker.frame = controller.view.frame
    groupPicker.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    controller.view.addSubview(groupPicker)
    
    let nav = UINavigationController(rootViewController: controller)    
    nav.modalPresentationStyle = .popover
    let popover = nav.popoverPresentationController!
    popover.permittedArrowDirections = .down
    popover.sourceView = button
    present(nav, animated: true, completion: nil)
    self.groupPickerController = nav
  }
}

extension CalendarController : CalendarDeledate {
  
  var groups: [CalendarGroup] {
    return mockGroups
  }
  
  func mockDayEvents(day: Date) -> [CalendarEvent] {
    let startHour = 8
    srand48(Int(day.timeIntervalSinceReferenceDate))
    func next() -> Int {
      return startHour + Int(round(drand48() * 8))
    }
    let numEvents = 2 + Int(round(drand48() * 10))
    var results = [CalendarEvent]()
    
    for i in  0..<numEvents {
      var length = Int(round(drand48() * 4)) + 1
      var start = day.add(next().hours)
      
      let allDayRand = Int(round(drand48() * 4))
      let isAllday = (i + allDayRand) % 4 == 0
      
      let maybeLunch = Int(round(drand48() * 10)) % 7
      let data : (String, String, String, String)
      if maybeLunch == 0 {
        data = mockLunches[i%2]
        start = day.add(12.hours)
      } else {
        let dataIdx = Int((drand48() * Double(mockEventData.count)))
        data = mockEventData[dataIdx]
      }
      results.append(
        CalendarEvent(title: data.0, start: start, end: start.add((length * 30).minutes), allDay: isAllday, additionalLines: [data.1, data.2, data.3], group: mockGroups[i % mockGroups.count])
      )
    }
    return results
  }
  
  // TODO(JEB): Create more nLink like test
  func events(from: Date, to: Date) -> [CalendarEvent] {
    var results = [CalendarEvent]()
    var current = from
    while current <= to {
      results.append(contentsOf: mockDayEvents(day: current))
      current = current.add(1.days)
    }
    return results
  }
  
  func selected(event: CalendarEvent) { 
  }
  
  func detailController(for event: CalendarEvent) -> UIViewController? {
    let viewController = UIViewController()
    viewController.view.backgroundColor = .white
    viewController.title = event.title
    let view = UILabel()
    view.text = event.title
    if UIDevice.current.userInterfaceIdiom == .phone {
      viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .done, target: viewController, action: #selector(close(sender:)))
    }
    viewController.view.addSubview(view)
    return UINavigationController(rootViewController: viewController)
  }
  
  @objc func closeGroupPicker() {
   groupPickerController?.dismiss(animated: true, completion: nil)
  }

}

