//
//  TabBarController.swift
//  nLinkCalendar
//
//  Created by Julian Hays on 2/22/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import Foundation
import UIKit

class TabBarController : UITabBarController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if let calController = viewControllers?[1] {
      self.selectedViewController = calController
    }
  }
  
}
