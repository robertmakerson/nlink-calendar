//
//  AgendaCell.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/24/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class AgendaCell: UITableViewCell {

  @IBOutlet weak var start : UILabel?
  
  @IBOutlet weak var end : UILabel?
  
  @IBOutlet weak var leader : UIView?
  
  @IBOutlet weak var title : UILabel?
  
  @IBOutlet weak var details : UITextView?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    details?.contentInset = .zero
    details?.textContainerInset = .zero
    details?.textContainer.lineFragmentPadding = 0
  }
}
