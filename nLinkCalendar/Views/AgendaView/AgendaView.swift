//
//  AgendaView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/24/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class AgendaView: PrimaryCalendarView {

  let weekBar = WeekBar()
  
  let pageContainer = CarouselView()
  
  override func setup() {
    addSubview(weekBar)
    addSubview(pageContainer)
    super.setup()
  }
  
  func configure() {
    pageContainer.delegate = self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    weekBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 50)
    pageContainer.frame = CGRect(x: 0, y: 50, width: bounds.width, height: bounds.height - 50)
    layoutDetails()
  }
  
  override func onChange(to day: Date) {
    let newPage = day.days(from: Date.today)
    pageContainer.moveTo(to: newPage)
  }
  
  override func onGroupsChange() {
    onChange(to: context.currentDay.value)
  }
}

extension AgendaView: CarouselViewDelegate {
  
  func pageView(for page: Int) -> UIView? {
    let day = Date.today.add(page.days)
    print("agenda: \(day)")
    let page = AgendaViewPage()
    page.onSelect = displayDetails
    context.dayEvents(for: day) { events in
      page.display(events: events)
    }
    return page
  }
  
  func moved(to page: Int) {
    change(day: Date.today.add(page.days))
  }
}
