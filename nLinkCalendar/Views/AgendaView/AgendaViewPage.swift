//
//  AgendaViewPage.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/24/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class AgendaViewPage: CalendarView {

  let table = UITableView()
  
  var events = [CalendarEvent]()
  
  var onSelect : (CalendarEvent) -> Void = { event in }
  
  override func setup() {
    table.dataSource = self
    table.delegate = self
    table.cellLayoutMarginsFollowReadableWidth = false
//    table.separatorStyle = .none
    table.register(UINib(nibName: "AgendaCell", bundle: nil), forCellReuseIdentifier: "AgendaCell")
    addSubview(table)
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    table.frame = bounds
  }
  
  func display(events: [CalendarEvent]) {
    self.events = events.sorted(by: { $0.allDay || $0.start.compare($1.start) == .orderedAscending })
    print("Rock and roll: \(events.count)")
    DispatchQueue.main.async {
      self.table.reloadData()
    }
  }
}

extension AgendaViewPage : UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return events.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let event = events[indexPath.row]
    let timeFormatter = DateFormatter()
    timeFormatter.dateStyle = .none
    timeFormatter.timeStyle = .short
    
    if let cell = table.dequeueReusableCell(withIdentifier: "AgendaCell") as? AgendaCell {
      cell.title?.text = event.title
      cell.leader?.backgroundColor = event.group.color
      if event.allDay {
        cell.start?.text = "All Day"
        cell.end?.text = ""
      } else {
        cell.start?.text = timeFormatter.string(from: event.start)
        cell.end?.text = timeFormatter.string(from: event.end)
      }
      var detailText = ""
      for line in event.additionalLines {
        if line.count > 0 {
          detailText += "\(line)\n"
        }
      }
      cell.details?.text = detailText
      return cell
    }
    return UITableViewCell()
  }

}

extension AgendaViewPage : UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 70
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    let event = events[indexPath.row]
    DispatchQueue.main.async {
      self.onSelect(event)
      tableView.deselectRow(at: indexPath, animated: true)
    }        
  }
  
}
