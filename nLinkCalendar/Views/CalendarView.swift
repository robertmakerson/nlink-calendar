//
//  CalendarSubview.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/5/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class CalendarView: ViewBase {
  
  var context: CalendarContext {
    return CalendarContext.current
  }
  
  override func setup() {
    context.currentDay
      .asObservable()
      .subscribe(onNext: onChange)
      .disposed(by: disposeBag)
    context.selectedGroups
      .asObservable()
      .subscribe(onNext: { [unowned self] groups in
        self.onGroupsChange()
      })
      .disposed(by: disposeBag)
  }
  
  func onChange(to day: Date) {
  }
  
  func onGroupsChange() {
  }
  
  func change(day: Date) {
    context.change(day: day)
  }
  
}
