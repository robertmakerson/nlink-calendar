//
//  CarouselView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/18/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

let sidePreload = 1

protocol CarouselViewDelegate : class {
  
  func pageView(for page: Int) -> UIView?
  
  func moved(to page: Int)
  
}

protocol CarouselPage {
  
  func willDisplay()
  
}

class CarouselView: ViewBase {

  private var currentPage = 0
  
  private let container = UIScrollView()
  
  var pageViews = [UIView?](repeating: nil, count: 1 + sidePreload * 2)
  
  weak var delegate: CarouselViewDelegate?
  
  override func setup() {
    container.backgroundColor = .clear
    container.isPagingEnabled = true
    container.delegate = self
    container.showsHorizontalScrollIndicator = false
    container.showsVerticalScrollIndicator = false
    addSubview(container)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    container.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height)
    container.contentSize = CGSize(width:  bounds.width * CGFloat(1 + 2 * sidePreload), height: bounds.height)
    container.contentOffset = CGPoint(x: CGFloat(sidePreload) * bounds.width, y: 0)
    for (idx, pageView) in pageViews.enumerated() {
      if let pageView = pageView {
        pageView.frame = CGRect(x: CGFloat(idx) * bounds.width, y: 0, width: bounds.width, height: bounds.height)
      }
    }
  }
  
  func reload() {
    for pageView in pageViews {
      if let pageView = pageView {
        pageView.removeFromSuperview()
      }
    }
    pageViews = [UIView?](repeating: nil, count: 1 + sidePreload * 2)
    moveTo(to: currentPage)
  }
  
  func moveTo(to newPage: Int) {
    // TODO(JEB): Should we only remove falloff?
    for pageView in pageViews {       
      if let pageView = pageView {
        pageView.removeFromSuperview()
      }
    }
    var idx = 0
    let oldViews = pageViews
    for pageNum in (newPage - sidePreload)...(newPage + sidePreload) {
      let oldIndex = idx + newPage - currentPage
      if oldIndex >= 0 && oldIndex < oldViews.count, let view = oldViews[oldIndex] {
        view.frame = CGRect(x: CGFloat(idx) * bounds.width, y: 0, width: bounds.width, height: bounds.height)
        container.addSubview(view)
        pageViews[idx] = view
      } else if let view = delegate?.pageView(for: pageNum) {
        view.frame = CGRect(x: CGFloat(idx) * bounds.width, y: 0, width: bounds.width, height: bounds.height)
        container.addSubview(view)
        pageViews[idx] = view
      }
      idx += 1
    }
    container.contentOffset = CGPoint(x: CGFloat(sidePreload) * bounds.width, y: 0)
    currentPage = newPage
  }
}

extension CarouselView : UIScrollViewDelegate {
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let currentPageOffset = CGFloat(sidePreload) * bounds.width
    if scrollView.contentOffset.x < currentPageOffset {
      moveTo(to: currentPage - 1)
      delegate?.moved(to: currentPage)
    } else if scrollView.contentOffset.x > currentPageOffset {
      moveTo(to: currentPage + 1)
      delegate?.moved(to: currentPage)
    }
  }
}
