//
//  DateTitleControl.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/12/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class DateTitleControl: CalendarView {

  var container = CarouselView()
  
  var onTap : (UIView) -> Void = {_ in}
  
  override func setup() {
    addSubview(container)
    container.delegate = self
    super.setup()
    container.moveTo(to: 0)
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    container.frame = self.bounds
  }
  
  @objc func tapped() {
    onTap(self)
  }

  override func onChange(to day: Date) {
    let newPage = day.startOfMonth.months(from: Date.today.startOfMonth)
    container.moveTo(to: newPage)
  }
  
}

extension DateTitleControl: CarouselViewDelegate {
  
  func pageView(for page: Int) -> UIView? {
    let pageView = DateTitlePage()
    let som = Date.today.startOfMonth
    let day = som.add(page.months)

    let formatter = DateFormatter()
    formatter.timeZone = Calendar.main.timeZone
    formatter.dateFormat = "MMMM"
    let monthName = formatter.string(from: day)

    pageView.monthLabel.text = monthName
    pageView.monthLabel.sizeToFit()

    formatter.dateFormat = "yyyy"
    pageView.yearLabel.text = formatter.string(from: day)
    pageView.yearLabel.frame = CGRect(x: pageView.monthLabel.bounds.width + 12, y: 0, width: 96, height: 30)
    return pageView
  }
  
  func moved(to page: Int) {
    let currentDay = context.currentDay.value
    let currentOffset = currentDay.startOfMonth.months(from: Date.today.startOfMonth)
    print(currentOffset)
    let pageChange = page - currentOffset
    print(pageChange)
    change(day: currentDay.startOfMonth.add(pageChange.months))
  }
}

class DateTitlePage : ViewBase {

  var monthLabel = UILabel()
  
  var yearLabel = UILabel()
  
  override func setup() {
    super.setup()
    monthLabel.frame = CGRect(x: 0, y: 0, width: 96, height: 30)
    monthLabel.font = UIFont.regular(size: 24)
    addSubview(monthLabel)
    yearLabel.frame = CGRect(x: 104, y: 0, width: 96, height: 30)
    yearLabel.font = UIFont.light(size: 24)
    addSubview(yearLabel)
  }
  
}
