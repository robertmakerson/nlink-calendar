//
//  DayAllDayEventContainer.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/29/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

let maxEventsPerRow = 3

class DayAllDayEventContainer: CalendarView {

  let allDayLabel = UILabel()
  
  var cells = [CellContainer]()
  
  var height : CGFloat = 0
  
  var onSelect : (CalendarEvent) -> Void = { event in }

  override func setup() {
    super.setup()
    clipsToBounds = true
    backgroundColor = DayViewConstants.allDayHeaderBackground
    allDayLabel.font = DayViewConstants.hourMarkerLabelFont
    allDayLabel.textAlignment = .right
    allDayLabel.textColor = DayViewConstants.allDayHeaderLabelColor
    allDayLabel.text = "All Day"
    addSubview(allDayLabel)    
  }
  
  func display(events: [CalendarEvent]) {
    for cell in cells {
      cell.removeFromSuperview()
    }
    cells.removeAll()
    height = 0
    for (idx, event) in events.enumerated() {
      let remaining = events.count - idx - 1
      let pos = idx % maxEventsPerRow
      var offset : CGFloat = 0
      var width : CGFloat = 1.0
      switch (pos) {
      case 0:
        offset = 0
        if remaining == 1 {
          width = 0.5
        } else if remaining > 1 {
          width = 1 / 3
        }
      case 1:
        if remaining  > 0 {
          offset = 1 / 3
          width = 1 / 3
        } else {
          offset = 0.5
          width = 0.5
        }
      default:
        width = 1 / 3
        offset = 2 / 3
      }
      let row = CGFloat(idx / maxEventsPerRow)
      let spec = CellSpec(row: row, offset: offset, width: width, event: event)
      if let cell = context.delegate?.cell(for: event) {
        let container = CellContainer(spec: spec, cell: cell)
        cells.append(container)
        container.onSelect = onSelect
        addSubview(container)
      }
      height = (spec.row + 1) * DayViewConstants.allDayRowHeight + 2
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    allDayLabel.frame = CGRect(x: 0, y: 0, width: DayViewConstants.timeLabelWidth, height: height)
    let cellOffset = DayViewConstants.eventXOffset
    let availableWidth = bounds.width - cellOffset
    for cell in cells {
      cell.frame = CGRect(x: cellOffset + availableWidth * cell.spec.offset, y: cell.spec.row * DayViewConstants.allDayRowHeight, width: availableWidth * cell.spec.width, height: DayViewConstants.allDayRowHeight)
    }
  }

}

class CellContainer : UIView {
  
  var spec: CellSpec
    
  var cell: UIView
  
  var onSelect : (CalendarEvent) -> Void = { event in }
  
  init(spec: CellSpec, cell: UIView) {
    self.spec = spec
    self.cell = cell
    super.init(frame: CGRect.zero)
    addSubview(cell)
    cell.isUserInteractionEnabled = false
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    cell.frame = CGRect(x: 0, y: 2, width: bounds.width - 2, height: bounds.height - 4)
  }
  
  @objc func tapped() {
    onSelect(spec.event)
  }
}

struct CellSpec {
  
  let row : CGFloat
  
  let offset : CGFloat
  
  let width : CGFloat
  
  let event : CalendarEvent
}
