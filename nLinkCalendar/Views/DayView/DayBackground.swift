//
//  DayBackground.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class DayBackground: ViewBase {

  var markers = [HourMarker]()

  override func setup() {    
    for hourIdx in 0..<25 {
      let marker = HourMarker()
      marker.frame = CGRect(x: 0, y: padding + CGFloat(hourIdx) * DayViewConstants.pixelPerHour, width: bounds.width,  height: 16)
      var hr = (hourIdx % 12)
      if hr == 0 {
        hr = 12
      }
      var ampm = "AM"
      if hourIdx > 12 && hourIdx < 24 {
        ampm = "PM"
      }
      marker.label.text = "\(hr) \(ampm)"
      addSubview(marker)
      markers.append(marker)
    }    
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    for marker in markers {
      marker.frame = CGRect(x: 0, y: marker.frame.minY, width: bounds.width, height: 15)
    }
  }
  
}

class HourMarker : UIView {
  
  let label = UILabel()
  
  let divider = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  func setup() {
    label.font = DayViewConstants.hourMarkerLabelFont
    label.textAlignment = .right
    label.textColor = DayViewConstants.hourMarkerColor
    addSubview(label)
    divider.backgroundColor = DayViewConstants.dividerColor
    addSubview(divider)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    label.frame = CGRect(x: 0, y: 0, width: DayViewConstants.timeLabelWidth, height: 16)
    divider.frame = CGRect(x: DayViewConstants.eventXOffset, y: 8, width: bounds.width - DayViewConstants.eventXOffset, height: 1)
  }
}
