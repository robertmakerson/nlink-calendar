//
//  DayView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/5/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class DayView: PrimaryCalendarView {
  
  let weekBar = WeekBar()
  
  let dayContainer = CarouselView()
    
  override func setup() {        
    addSubview(weekBar)
    addSubview(dayContainer)    
    super.setup()
  }
  
  func configure() {
    dayContainer.delegate = self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    weekBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 50)
    layoutDetails()
  }
  
  override func layoutDetails() {
    super.layoutDetails()
    let width = detailsVisible ? bounds.width / 2  : bounds.width
    dayContainer.frame = CGRect(x: 0, y: 50, width: width, height: bounds.height - 50)
  }
  
  override func onChange(to day: Date) {
    let newPage = day.days(from: Date.today)
    dayContainer.moveTo(to: newPage)
  }
  
  override func onGroupsChange() {
    dayContainer.reload()
  }

}

extension DayView : CarouselViewDelegate {
  
  func pageView(for page: Int) -> UIView? {
    let pageView = DayViewPage()
    pageView.onSelect = displayDetails
    let day = Date.today.add(page.days)
    pageView.day = day
    context.dayEvents(for: day, callback: pageView.display)
    return pageView
  }
  
  func moved(to page: Int) {
    change(day: Date.today.add(page.days))
  }    
}
