//
//  DayViewConstants.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/14/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

struct DayViewConstants {
  
  static var pixelPerHour : CGFloat = {
    return UIDevice.current.userInterfaceIdiom == .pad ? 72 : 36
  }()
  
  static var pixelPerDay : CGFloat = {
    return pixelPerHour * 24
  }()
  
  static let hourMarkerLabelFont = UIFont.light(size: 12)
  
  static let hourMarkerColor = UIColor.gray(50)
  
  static let dividerColor = UIColor.gray(200)
  
  static let nowMarkerLabelFont = UIFont.light(size: 11)
  
  static let nowMarkerColor = UIColor.mdtOrange
  
  static let eventTextColor = UIColor.gray(50)
  
  static let eventXOffset : CGFloat = 48
  
  static let timeLabelWidth = eventXOffset - 6
  
  static let headerBackgroundColor = UIColor.gray(247)
  
  static let allDayHeaderBackground = UIColor.gray(210)
  
  static let allDayHeaderLabelColor = UIColor.gray(0)
  
  static let allDayRowHeight : CGFloat = 30
}
