//
//  DayViewEventContainer.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/14/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

let padding : CGFloat = 8

class DayViewEventContainer: CalendarView {
  
  var eventViews = [EventCellContainer]()
  
  var onSelect : (CalendarEvent) -> Void = { event in }
  
  override func setup() {
    super.setup()
    backgroundColor = .clear  
  }
  
  func display(events: [CalendarEvent]) {
    for child in subviews {
      child.removeFromSuperview()
    }
    eventViews.removeAll()
    let events = events.sorted { first, second in
      if first.start == second.start {
        return first.end.compare(second.end) == .orderedDescending
      }
      return first.start.compare(second.start) == .orderedAscending
    }
    var specs = [EventViewSpec]()
    for event in events {
      let spec = EventViewSpec(event: event)
      for other in specs {
        if spec.range.overlaps(other: other.range) {
          spec.overlaps.insert(other)
          other.overlappedBy.insert(spec)
        }
      }
      specs.append(spec)
      if let cell = context.delegate?.cell(for: spec.event) {
        let view = EventCellContainer(spec: spec, cell: cell)
        addSubview(view)
        eventViews.append(view)
        view.onTap = {
          self.onSelect(view.event)
        }
      }
    }
    let processContext = ProcessContext()
    for spec in specs {
      processLeft(spec: spec, context: processContext)
      processRight(spec: spec, context: processContext)
      processOffset(spec: spec, context: processContext)
    }
    layoutEvents()
  }
  
  func processLeft(spec: EventViewSpec, context: ProcessContext) {
    if context.leftProcessed.contains(spec) {
      return
    }
    var toLeft = 0
    for other in spec.overlaps {
      processLeft(spec: other, context: context)
      toLeft = max(toLeft, other.leftCount + 1)
    }
    spec.leftCount = toLeft
    context.leftProcessed.insert(spec)
  }
  
  func processRight(spec: EventViewSpec, context: ProcessContext) {
    if context.rightProcessed.contains(spec) {
      return
    }
    var toRight = 0
    for other in spec.overlappedBy {
      processRight(spec: other, context: context)
      toRight = max(toRight, other.rightCount + 1)
    }
    spec.rightCount = toRight
    context.rightProcessed.insert(spec)
  }
  
  func processOffset(spec: EventViewSpec, context: ProcessContext) {
    if spec.rightCount > 0 {
      // Normal Calc
      spec.width = 1.0 / Double(spec.leftCount + spec.rightCount + 1)
      spec.offset = spec.width * Double(spec.leftCount)
    } else {
      // Fill Calc
      var maxOffset = 0.0
      for other in spec.overlaps {
        processOffset(spec: other, context: context)
        maxOffset = max(maxOffset, min(1.0, other.offset + other.width))
      }
      spec.offset = maxOffset
      spec.width = 1.0 - maxOffset
    }
    context.offsetProcessed.insert(spec)
  }
    
  override func layoutSubviews() {
    super.layoutSubviews()
    layoutEvents()
  }

  func layoutEvents() {
    var availableWidth = bounds.width - 2
    availableWidth = max(availableWidth, 1)
    for eventView in eventViews {
      let eventWidth = availableWidth * CGFloat(eventView.spec.width)
      let xOffset = availableWidth * CGFloat(eventView.spec.offset)
      let startY = CGFloat(eventView.event.start.minutesIntoDay) / 60 * DayViewConstants.pixelPerHour
      let endY = CGFloat(eventView.event.end.minutesIntoDay) / 60 * DayViewConstants.pixelPerHour
      eventView.frame = CGRect(x: xOffset, y: padding + startY, width: eventWidth, height: endY - startY)
    }
  }

  class ProcessContext {
    var leftProcessed = Set<EventViewSpec>()
    var rightProcessed = Set<EventViewSpec>()
    var offsetProcessed = Set<EventViewSpec>()
  }
 
  class EventViewSpec : Hashable {

    var event: CalendarEvent
    
    var range: DateRange
    
    var leftCount: Int = 0
    
    var rightCount: Int = 0
    
    var offset: Double = 0
    
    var width: Double = 0
    
    var overlappedBy = Set<EventViewSpec>()
    
    var overlaps = Set<EventViewSpec>()
    
    var widthPercentage : Double {
      return 1.0 / Double(leftCount + 1 + rightCount)
    }
    
    init(event: CalendarEvent) {
      self.event = event
      self.range = DateRange(start: event.start, end: event.end)
    }
    
    static func ==(lhs: DayViewEventContainer.EventViewSpec, rhs: DayViewEventContainer.EventViewSpec) -> Bool {
      return lhs.event.id == rhs.event.id
    }
    
    var hashValue: Int {
      return event.id.hashValue
    }
  }
  
  class EventCellContainer : UIView {
    
    var event: CalendarEvent
    
    var spec: EventViewSpec
    
    var cell: UIView
    
    var onTap : () -> Void = {}
    
    init(spec: EventViewSpec, cell: UIView) {
      self.spec = spec
      self.event = spec.event
      self.cell = cell
      super.init(frame: CGRect.zero)      
      addSubview(cell)
      cell.isUserInteractionEnabled = false
      addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
    }
    
    required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
      super.layoutSubviews()
      cell.frame = bounds
    }
    
    @objc func tapped() {
          onTap()
        }
  }
  
}
