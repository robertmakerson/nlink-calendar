//
//  DayViewPage.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class DayViewPage: CalendarView {

  let scrollView = UIScrollView()
  
  let allDayeventContainer = DayAllDayEventContainer()
  
  let eventContainer = DayViewEventContainer()
  
  var background = DayBackground()
  
  var nowMarker = NowMarker()
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      eventContainer.onSelect = onSelect
      allDayeventContainer.onSelect = onSelect
    }
  }
  
  var day = Date.today {
    didSet {
      if day.startOfDay == Date.today {
        addNowMarker()
      }
    }
  }
  
  override func setup() {
    addSubview(allDayeventContainer)
    addSubview(scrollView)
    scrollView.showsVerticalScrollIndicator = false
    scrollView.showsHorizontalScrollIndicator = false
    scrollView.addSubview(background)
    scrollView.addSubview(eventContainer)
    eventContainer.onSelect = onSelect
    allDayeventContainer.onSelect = onSelect
    super.setup()
  }
  
  func display(events: [CalendarEvent]) {
    allDayeventContainer.display(events: events.filter( { $0.allDay }))
    eventContainer.display(events: events.filter( { !$0.allDay }))
    layoutSubviews()
  }
  
  private func addNowMarker() {
    scrollView.addSubview(nowMarker)
    updateNow()
  }
  
  private func updateNow() {
    let formatter = DateFormatter()
    formatter.dateFormat = "h:mm a"
    nowMarker.label.text = formatter.string(from: Date())
    let offset = CGFloat(Date().minutesIntoDay) * DayViewConstants.pixelPerHour / 60
    nowMarker.frame = CGRect(x: 0, y: offset + padding, width: eventContainer.bounds.width, height: 15)
    DispatchQueue.main.asyncAfter(deadline: .now() + 30.0) { [weak self] in
      self?.updateNow()
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    
    allDayeventContainer.frame = CGRect(x: 0, y: 0, width: bounds.width, height: allDayeventContainer.height)
        
    scrollView.frame = CGRect(x: 0, y: allDayeventContainer.height, width: bounds.width, height: bounds.height - allDayeventContainer.height)
    eventContainer.frame = CGRect(x: DayViewConstants.eventXOffset, y: 8, width: scrollView.bounds.width, height: DayViewConstants.pixelPerDay)
    scrollView.contentSize = CGSize(width: scrollView.bounds.width, height: DayViewConstants.pixelPerDay + 28)
    
    background.frame = CGRect(x: 0, y: 0, width: bounds.width, height: DayViewConstants.pixelPerDay + 28)
    nowMarker.frame = CGRect(x: 0, y: nowMarker.frame.minY, width: eventContainer.bounds.width, height: 15)
    scrollNow()
  }
  
  func scrollNow() {
    var offset = DayViewConstants.pixelPerHour * 7
    if day.startOfDay == Date.today {
      offset = CGFloat(Date().minutesIntoDay) * DayViewConstants.pixelPerHour / 60
    }
    offset = min(offset, DayViewConstants.pixelPerDay - bounds.height)
    scrollView.contentOffset = CGPoint(x: 0, y: offset)
  }
  
}

class NowMarker : UIView {
  
  let label = UILabel()
  
  let divider = UIView()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  func setup() {
    label.font = DayViewConstants.nowMarkerLabelFont
    label.textAlignment = .right
    label.textColor = DayViewConstants.nowMarkerColor
    addSubview(label)
    divider.backgroundColor = DayViewConstants.nowMarkerColor
    addSubview(divider)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    label.frame = CGRect(x: 0, y: 0, width: DayViewConstants.timeLabelWidth, height: 16)
    divider.frame = CGRect(x: DayViewConstants.eventXOffset, y: 8, width: bounds.width - DayViewConstants.eventXOffset, height: 1)
  }
}
