//
//  EventCell.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/15/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

let eventCellPadding : CGFloat = 6

class EventCell: UIView {

  let event : CalendarEvent
  
  private let titleLabel = UILabel()
  
  private let detailsView = UITextView()
  
  init(event: CalendarEvent) {
    self.event = event
    super.init(frame: CGRect.zero)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func setup() {
    backgroundColor = event.group.color.withAlphaComponent(0.2)
    layer.borderColor = event.group.color.cgColor
    
    let leader = UIView(frame: CGRect(x: 0, y: 0, width: 2, height: bounds.height))
    leader.autoresizingMask = .flexibleHeight
    leader.backgroundColor = event.group.color
    addSubview(leader)
    
    titleLabel.textColor = DayViewConstants.eventTextColor
    titleLabel.font = UIFont.regular(size: 12)
    addSubview(titleLabel)
    
    if event.allDay || event.end.minutes(from: event.start) < 60 {
      var title = event.title
      if let firstLine = event.additionalLines.first, !firstLine.isEmpty {
        title += " - \(firstLine)"
      }
      titleLabel.text = title
    } else {
      titleLabel.text = event.title
      var detailText = ""
      for line in event.additionalLines {
        if line.count > 0 {
          detailText += "\(line)\n"
        }
      }
      detailsView.textColor = DayViewConstants.eventTextColor
      detailsView.font = UIFont.light(size: 12)
      detailsView.text = detailText
      detailsView.backgroundColor = .clear
      detailsView.isEditable = false
      detailsView.isScrollEnabled = false
      detailsView.contentInset = .zero
      detailsView.textContainerInset = .zero
      detailsView.textContainer.lineFragmentPadding = 0
      
      addSubview(detailsView)
    }

  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    titleLabel.frame = CGRect(x: eventCellPadding , y: eventCellPadding, width: bounds.width - eventCellPadding * 2, height: 12)
    let detailOffset = titleLabel.frame.maxY + eventCellPadding / 2
    detailsView.frame = CGRect(x: eventCellPadding, y: detailOffset, width: bounds.width - eventCellPadding * 2, height: bounds.height - detailOffset - eventCellPadding)
  }
  
}
