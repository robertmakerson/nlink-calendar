//
//  EventDetailView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/28/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class EventDetailView: ViewBase {

  var current: UIView?
  
  override func setup() {
    super.setup()
    let divider = UIView(frame: CGRect(x: -1, y: 0, width: 1, height: bounds.height))
    divider.autoresizingMask = .flexibleHeight
    divider.backgroundColor = DayViewConstants.dividerColor
    addSubview(divider)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    current?.frame = CGRect(x: 1, y: 0, width: bounds.width - 1, height: bounds.height)
  }
  
  func display(view: UIView) {
    current?.removeFromSuperview()
    current = view
    view.frame = CGRect(x: 1, y: 0, width: bounds.width - 1, height: bounds.height)
    addSubview(view)
  }

}
