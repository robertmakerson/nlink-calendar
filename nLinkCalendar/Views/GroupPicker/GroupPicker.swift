//
//  GroupPicker.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/18/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class GroupPicker: CalendarView {

  let tableView = UITableView()
  
  override func setup() {
    super.setup()
    addSubview(tableView)
    tableView.delegate = self
    tableView.dataSource = self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    tableView.frame = bounds
  }
  
  private func group(for indexPath: IndexPath) -> CalendarGroup? {
    switch indexPath.section {
    case 0: return (context.delegate?.groups ?? [])[indexPath.row]
    case 1: return context.nativeCalendars[indexPath.row]
    default: return nil
    }
  }
  
}

extension GroupPicker: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0: return context.delegate?.groups.count ?? 0
    case 1: return context.nativeCalendars.count
    default: return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = UITableViewCell()
    if let group = group(for: indexPath) {
      cell.textLabel?.text = group.name
      if context.selectedGroups.value.contains(group) {
        cell.accessoryType = .checkmark
      } else {
        cell.accessoryType = .none
      }
    }
    return cell
  }
}

extension GroupPicker: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    switch (section) {
    case 0: return "Custom View(s)"
    case 1: return "iCal View(s)"
    default: return nil
    }
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let group = group(for: indexPath) {
      if context.selectedGroups.value.contains(group) {
        context.selectedGroups.value.remove(group)
      } else {
        context.selectedGroups.value.insert(group)
      }
      tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    tableView.deselectRow(at: indexPath, animated: false)
  }

}
