//
//  MonthPicker.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/19/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

let months = [
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
]

class MonthPicker : CalendarView {
  
  let container  = UIView()
  
  let year = UILabel()
  
  let back = UIButton()
  
  let forward = UIButton()
  
  var monthButtons = [UIButton]()
  
  var curYear = 0
  
  override func setup() {
    super.setup()
    clipsToBounds = true
    backgroundColor = .clear
    container.backgroundColor = .white
    container.layer.borderColor = UIColor.gray(150).cgColor
    container.layer.borderWidth = 0.5
    container.layer.shadowColor = UIColor.lightGray.cgColor
    container.layer.shadowOffset = CGSize(width: 2, height: 2)
    container.layer.shadowOpacity = 0.1
    container.layer.cornerRadius = 10
    
    back.setImage(UIImage(named: "Back"), for: .normal)
    back.contentVerticalAlignment = .fill
    back.contentHorizontalAlignment = .fill
    back.imageEdgeInsets =  UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    back.rx.tap
      .subscribe(onNext: {
        self.curYear = self.curYear - 1
        self.year.text =  "\(self.curYear)"
      })
      .disposed(by: disposeBag)
    
    forward.setImage(UIImage(named: "Forward Arrow"), for: .normal)
    forward.contentVerticalAlignment = .fill
    forward.contentHorizontalAlignment = .fill
    forward.imageEdgeInsets =  UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    forward.rx.tap
      .subscribe(onNext: {
        self.curYear = self.curYear + 1
        self.year.text =  "\(self.curYear)"
      })
      .disposed(by: disposeBag)
    
    container.addSubview(back)
    container.addSubview(forward)
    
    addSubview(container)
    year.font = UIFont.bold(size: 26)
    year.textAlignment = .center
    container.addSubview(year)
    
    for (idx, month) in months.enumerated() {
      let button = UIButton()
      button.setTitle(month, for: .normal)
      button.titleLabel?.font = UIFont.bold(size: 16)
      button.setTitleColor(.black, for: .normal)
      button.layer.cornerRadius = 20
      monthButtons.append(button)
      container.addSubview(button)
      button.rx.tap.subscribe(onNext: {
        let cal = Calendar.current
        self.selectMonth(month: idx)
        let comps  = DateComponents(year: self.curYear, month: idx + 1)
        if let date = cal.date(from: comps) {
          self.change(day: date)
        }
        self.hide()
      })
      .disposed(by: disposeBag)
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    container.frame = CGRect(x: 0, y: 0, width: bounds.width - 10, height: 240)
    year.frame = CGRect(x: (bounds.width - 80) / 2, y: 20, width: 80, height: 30)
    back.frame = CGRect(x:  year.frame.minX - 30, y: 22, width: 30, height: 30)
    forward.frame = CGRect(x:  year.frame.maxX, y: 22, width: 30, height: 30)
    
    let yOffset = Int(year.frame.maxY + 16)
    let buttonSide = 40
    let columnWidth = 80
    let buttonMargin = (columnWidth - buttonSide) / 2
    let xOffset = (Int(bounds.width) - (columnWidth * 3)) / 2
    for (idx, button) in monthButtons.enumerated() {
      let col = idx % 3
      let row = idx / 3
      button.frame = CGRect(x: xOffset + buttonMargin + col * columnWidth, y: yOffset + row * buttonSide, width: buttonSide, height: buttonSide)
    }
  }
  
  override func onChange(to day: Date) {
    curYear = Calendar.current.component(.year, from: day)
    year.text =  "\(curYear)"
    let curMonth = Calendar.current.component(.month, from: day)
    selectMonth(month: curMonth - 1)
  }
  
  func selectMonth(month: Int) {
    for (idx, monthButton) in monthButtons.enumerated() {
      if month == idx {
        monthButton.backgroundColor = UIColor.mdtMediumBlue
        monthButton.setTitleColor(.white, for: .normal)
      } else {
        monthButton.backgroundColor = .clear
        monthButton.setTitleColor(.black, for: .normal)
      }
    }
  }
  
  func toggle() {
    if bounds.height == 0 {
      show()
    } else {
      hide()
    }
  }
  
  func show() {
    UIView.animate(withDuration: 0.2) {
      var frame = self.frame
      frame.size.height = self.container.bounds.height + 5
      self.frame = frame
    }
  }
  
  func hide() {
    UIView.animate(withDuration: 0.2) {
      var frame = self.frame
      frame.size.height = 0
      self.frame = frame
    }
  }
}

