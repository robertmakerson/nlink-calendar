//
//  PrimaryCalendarView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 5/2/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class PrimaryCalendarView: CalendarView {

  let detailContainer = EventDetailView()
  
  let detailDismiss = UIButton()
  
  var detailsVisible = false
  
  override func setup() {
    addSubview(detailContainer)
    addSubview(detailDismiss)
    let imageView = UIImageView(image: UIImage.init(named: "Collapse Detail"))
    imageView.frame = CGRect(x: 0, y: 0, width: 38, height: 38)
    imageView.contentMode = .scaleAspectFit
    detailDismiss.addSubview(imageView)
    detailDismiss.rx.tap
      .subscribe(onNext: self.hideDetails)
      .disposed(by: disposeBag)
    detailDismiss.isHidden = true
    super.setup()
  }
  
  // MARK: Actions
  
  func displayDetails(event: CalendarEvent) {
    detailsVisible = true
    if UIDevice.current.userInterfaceIdiom != .pad {
      context.displayDetailModal(for: event)
      return
    }
    guard let viewController = context.detailController(for: event) else { return }
    viewController.view.frame = CGRect(x: 0, y: 0, width: self.detailContainer.frame.width, height: self.detailContainer.frame.height)
    
    detailContainer.display(view: viewController.view)
    UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
      self.layoutDetails()
      self.detailDismiss.isHidden = false
    }, completion: nil)
  }
  
  func hideDetails() {
    detailsVisible = false
    UIView.animate(withDuration: 0.4, delay: 0, options: UIViewAnimationOptions.layoutSubviews, animations: {
      self.layoutDetails()
      self.detailDismiss.isHidden = true
    }, completion: nil)
  }
  
  func layoutDetails() {
    let width = detailsVisible ? bounds.width / 2  : bounds.width    
    detailContainer.frame = CGRect(x: width, y: 50, width: bounds.width / 2, height: bounds.height - 50)
    detailDismiss.frame = CGRect(x: width - 20, y: 54, width: 60, height: 60)
  }
  
}
