//
//  ScheduleAllDayEventContainer.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/17/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class ScheduleAllDayEventContainer: ViewBase {
  
  var groups = [CalendarGroup]() {
    didSet {
      setupGroups()
    }
  }
  
  var eventsByGroup = [CalendarGroup: [CalendarEvent]]()
  
  var groupCells = [GroupCell]()
  
  var height : CGFloat {
    var height : CGFloat = 0
    for cell in groupCells {
      height = max(cell.height, height)
    }
    return height
  }
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      for cell in groupCells {
        cell.onSelect = onSelect
      }
    }
  }
  
  override func setup() {
    super.setup()
    clipsToBounds = true
    backgroundColor = DayViewConstants.allDayHeaderBackground
    setupGroups()
  }
  
  func setupGroups() {
    for view in groupCells {
      view.removeFromSuperview()
    }
    groupCells.removeAll()
    for group in groups { 
      let cell = GroupCell()
      if let events = eventsByGroup[group] {
        cell.display(events: events)
      }
      cell.onSelect = onSelect
      groupCells.append(cell)
      addSubview(cell)
      
//      page.onSelect = onSelect
//      if let events = eventsByGroup[group] {
//        page.eventContainer.display(events: events)
//      }
//      vertScrollView.addSubview(page)
//      groupContainers.append(page)
    }
    layoutSubviews()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let groupWidth = (superview?.bounds.width ?? 0) / 3.0
    for (idx, cell) in groupCells.enumerated() {
      cell.frame = CGRect(x: groupWidth * CGFloat(idx), y: 0, width: groupWidth, height: height)
    }
  }
  
  func display(events: [CalendarEvent]) {
    eventsByGroup.removeAll()
    for event in events {
      if eventsByGroup[event.group] != nil {
        eventsByGroup[event.group]!.append(event)
      } else {
        eventsByGroup[event.group] = [event]
      }
    }
    setupGroups()
  }
  
  class GroupCell : CalendarView {
    
    let allDayLabel = UILabel()
    
    var eventCells = [UIView]()
    
    var height : CGFloat = 0
    
    var onSelect : (CalendarEvent) -> Void = { event in }
    
    override func setup() {
      super.setup()
      allDayLabel.font = DayViewConstants.hourMarkerLabelFont
      allDayLabel.textAlignment = .right
      allDayLabel.textColor = DayViewConstants.allDayHeaderLabelColor
      allDayLabel.text = "All Day"
      addSubview(allDayLabel)
    }
    
    func display(events: [CalendarEvent]) {
      for cell in eventCells {
        cell.removeFromSuperview()
      }
      
      for event in events {
        if let cell = context.delegate?.cell(for: event) {
          eventCells.append(cell)
          let tapRecog = UITapGestureRecognizer()
          cell.addGestureRecognizer(tapRecog)
          tapRecog.rx.event.bind(onNext: { [unowned self] recorgnizer in
            self.onSelect(event)
          }).disposed(by: disposeBag)
          addSubview(cell)
        }
      }
      height = CGFloat(events.count) * DayViewConstants.allDayRowHeight + 2
      layoutSubviews()
    }
    
    override func layoutSubviews() {
      super.layoutSubviews()
      for (idx, cell) in eventCells.enumerated()  {
        cell.frame = CGRect(x: DayViewConstants.eventXOffset, y: CGFloat(idx) * DayViewConstants.allDayRowHeight + 2, width: bounds.width - DayViewConstants.eventXOffset, height: DayViewConstants.allDayRowHeight - 4)
      }
      allDayLabel.frame = CGRect(x: 0, y: 0, width: DayViewConstants.timeLabelWidth, height: height)
    }
  }
}


