//
//  ScheduleView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/12/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class ScheduleView: PrimaryCalendarView {
  
  let weekBar = WeekBar()
  
  let pageContainer = ScheduleViewPage()
  
  override func setup() {    
    addSubview(weekBar)
    pageContainer.onSelect = displayDetails
    addSubview(pageContainer)
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    weekBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 50)
    pageContainer.frame = CGRect(x: 0, y: 50, width: bounds.width, height: bounds.height - 50)
    layoutDetails()
  }

  override func onChange(to day: Date) {
    pageContainer.groups = (context.delegate?.groups ?? []).filter({
      context.selectedGroups.value.contains($0)
    })
    pageContainer.day = day    
    context.dayEvents(for: day, callback: pageContainer.display)
  }
  
  override func onGroupsChange() {
    onChange(to: context.currentDay.value)
  }
 
}
  

