//
//  ScheduleViewPage.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/12/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class ScheduleViewGroupContainer: CalendarView {

  let backGround = DayBackground()
  
  let eventContainer = DayViewEventContainer()
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      eventContainer.onSelect = onSelect
    }
  }
  
  override func setup() {
    super.setup()
    addSubview(backGround)
    eventContainer.onSelect = onSelect
    addSubview(eventContainer)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    backGround.frame = bounds
    eventContainer.frame = CGRect(x: DayViewConstants.eventXOffset, y: 8, width: bounds.width - DayViewConstants.eventXOffset, height: DayViewConstants.pixelPerDay)
  }
}
