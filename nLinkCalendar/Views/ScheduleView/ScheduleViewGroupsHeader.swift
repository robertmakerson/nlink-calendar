//
//  ScheduleViewGroupsHeader.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/13/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class ScheduleViewGroupsHeader : ViewBase {

  var groups = [CalendarGroup]() {
    didSet {
      updateGroups()
    }
  }
  
  var groupViews = [UIView]()
  
  override func setup() {
    super.setup()
    updateGroups()
  }
  
  func updateGroups() {
    for view in groupViews {
      view.removeFromSuperview()
    }
    groupViews.removeAll()
    for group in groups {
      let view = UILabel()
      view.font = UIFont.medium(size: 20)
      view.textColor = .black
      view.text = group.name
      view.textAlignment = .center
      addSubview(view)
      groupViews.append(view)
    }
    layoutSubviews()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let groupWidth = bounds.width / 3.0
    for (idx, groupView) in groupViews.enumerated() {
      groupView.frame = CGRect(x: CGFloat(idx) * groupWidth, y: 0, width: groupWidth, height: bounds.height)
    }
  }

}
