//
//  ScheduleViewPage.swift
//  nLinkCalendar
//
//  Created by John Bailey on 4/13/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class ScheduleViewPage : CalendarView {

  let horzScrollView = UIScrollView()
  
  let vertScrollView = UIScrollView()
  
  var titleHeader = ScheduleViewGroupsHeader()
  
  let allDayContainer = ScheduleAllDayEventContainer()
  
  var groups = [CalendarGroup]()
  
  var eventsByGroup = [CalendarGroup: [CalendarEvent]]()
  
  var groupContainers = [ScheduleViewGroupContainer]()
  
  var day = Date.today
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      for groupContainer in groupContainers {
        groupContainer.onSelect = onSelect        
      }
      allDayContainer.onSelect = onSelect
    }
  }
  
  override func setup() {
    super.setup()
    addSubview(horzScrollView)
    addSubview(vertScrollView)
    horzScrollView.addSubview(titleHeader)
    horzScrollView.addSubview(allDayContainer)
    horzScrollView.addSubview(vertScrollView)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let groupWidth = bounds.width / 3.0
    horzScrollView.frame = bounds
    titleHeader.frame = CGRect(x: 0, y: 0, width: bounds.width, height: 60)
    allDayContainer.frame = CGRect(x: 0, y: 60, width: groupWidth * CGFloat(groupContainers.count), height: allDayContainer.height)
    let eventContainerOffset = allDayContainer.frame.maxY
    vertScrollView.frame = CGRect(x: 0, y: eventContainerOffset, width: groupWidth * CGFloat(groupContainers.count), height: bounds.height - eventContainerOffset)
    for (idx, groupView) in groupContainers.enumerated() {
      groupView.frame = CGRect(x: CGFloat(idx) * groupWidth, y: 0, width: groupWidth, height: DayViewConstants.pixelPerDay + 28)
    }
    horzScrollView.contentSize = CGSize(width: groupWidth * CGFloat(groupContainers.count), height: bounds.height)
    vertScrollView.contentSize = CGSize(width: groupWidth * CGFloat(groupContainers.count), height: DayViewConstants.pixelPerDay + 28)
    scrollNow()
  }
  
  func scrollNow() {
    var offset = DayViewConstants.pixelPerHour * 7
    if day.startOfDay == Date.today {
      offset = CGFloat(Date().minutesIntoDay) * DayViewConstants.pixelPerHour / 60
    }
    offset = min(offset, DayViewConstants.pixelPerDay - bounds.height)
    vertScrollView.contentOffset = CGPoint(x: 0, y: offset)
  }
  
  func setupGroups() {
    for view in groupContainers {
      view.removeFromSuperview()
    }
    groupContainers.removeAll()
    titleHeader.groups = groups
    allDayContainer.groups = groups
    for group in groups {
      let page = ScheduleViewGroupContainer()
      page.onSelect = onSelect
      if let events = eventsByGroup[group] {
        page.eventContainer.display(events: events)
      }
      vertScrollView.addSubview(page)
      groupContainers.append(page)
    }
    layoutSubviews()
  }
  
  
  func display(events: [CalendarEvent]) {
    allDayContainer.display(events: events.filter({ $0.allDay }))
    eventsByGroup.removeAll()
    for event in events.filter({ !$0.allDay }) {
      if eventsByGroup[event.group] != nil {
        eventsByGroup[event.group]!.append(event)
      } else {
       eventsByGroup[event.group] = [event]
      }
    }
    setupGroups()
  }
  
}
