//
//  ViewBase.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/18/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit
import RxSwift

class ViewBase: UIView {

  let disposeBag = DisposeBag()
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
    setup()
  }
  
  func setup() {
  }

}
