//
//  WeekBarCellContainer.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/19/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class WeekBar: CalendarView {
  
  let carousol = CarouselView()
  
  let divider = UIView()
  
  override func setup() {
    carousol.delegate = self    
    addSubview(carousol)    
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    carousol.frame = bounds    
  }
    
  func updateSelection() {
    for page in carousol.pageViews {
      if let page = page as? WeekBarCellContainer {
        let dow = Calendar.current.component(.weekday, from: context.currentDay.value)
        page.selectedCell = dow - 1
      }
    }
  }
    
  override func onChange(to day: Date) {
    // TODO(JEB): Remove bangs
    carousol.moveTo(to: day.startOfWeek.weeks(from: Date.today.startOfWeek))
    updateSelection()
  }

} 

extension WeekBar: CarouselViewDelegate {
  
  func pageView(for page: Int) -> UIView? {
    let container = WeekBarCellContainer()
    container.selectedDate = Date.today.add(page.weeks)
    container.tapped = change
    container.configDays()
    return container
  }
  
  func moved(to page: Int) {
    let currentDay = context.currentDay.value
    let currentOffset = currentDay.startOfWeek.weeks(from: Date.today.startOfWeek)
    let pageChange = page - currentOffset
    change(day: currentDay.add(pageChange.weeks))
  }

}
