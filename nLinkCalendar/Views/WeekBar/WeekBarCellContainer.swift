//
//  WeekBarView.swift
//  nLinkCalendar
//
//  Created by Julian Hays on 2/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import Foundation
import UIKit

class WeekBarCellContainer : ViewBase {
  
  var cells = [WeekBarDayCell]()
  
  let divider = UIView()
  
  var  cellsSelectable = true
  
  public var tapped: (Date)->() = { _ in }
  
  public var selectedDate = Date.today {
    didSet {
      configDays()
    }
  }
  
  let dayNameFormatter = DateFormatter()
  
  let dayNumFormatter = DateFormatter()
  
  var useLongDayName = false
  
  var cellOffset : CGFloat = 0 {
    didSet {
      layoutSubviews()
    }
  }
  
  var selectedCell = -1 {
    didSet {
      if selectedCell < 0 {
        clearSelect()
      }
      else if selectedCell < cells.count {
        selectCell(cell: cells[selectedCell])
      }
    }
  }
  
  override func setup() {
    backgroundColor = DayViewConstants.headerBackgroundColor
    dayNameFormatter.dateFormat = "EEE"
    dayNumFormatter.dateFormat = "d"    
    if UIDevice.current.userInterfaceIdiom == .pad  {
      useLongDayName = true
    }
    for _ in 0..<7 {
      let cell = WeekBarDayCell()
      cells.append(cell)
      addSubview(cell)
    }
    
    divider.backgroundColor = DayViewConstants.dividerColor
    addSubview(divider)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let cellWidth = (bounds.width - cellOffset) / 7
    for (idx, cell) in cells.enumerated() {
      cell.frame = CGRect(x: cellOffset + CGFloat(idx) * cellWidth, y: 0, width: cellWidth, height: bounds.height)
    }
    divider.frame = CGRect(x: 0, y: bounds.height - 1, width: bounds.width, height: 1)
  }

  func configDays() {
    let cal = Calendar.current
    let firstDayOfWeek = selectedDate.startOfWeek 
    for idx in 0...6 {
      let daysToAdd = idx
      var dateComponent = DateComponents()
      dateComponent.day = daysToAdd
      if let day = cal.date(byAdding: dateComponent, to: firstDayOfWeek) {
        setup(cell: cells[idx], for: day, selected: false, weekend: idx == 0 || idx == 6)
      }
    }
  }
  
  private func clearSelect() {
    for other in cells {
      other.selected = false
    }
  }
  
  private func selectCell(cell: WeekBarDayCell) {
    clearSelect()
    cell.selected = true
  }
  
  private func setup(cell: WeekBarDayCell, for date: Date, selected : Bool = false, weekend : Bool = false) {
    cell.weekend = weekend
    let dayName = dayNameFormatter.string(from: date)
    var firstLetter = ""
    if let f = dayName.first {
      firstLetter = String(f)
    }
    cell.dayNumber.text = dayNumFormatter.string(from: date)
    cell.dayName.text = useLongDayName ? dayName : firstLetter
    cell.date = date
    cell.selected = selected
    if cellsSelectable {
      cell.tapped = { [unowned self] cell in
        self.selectCell(cell: cell)
        self.tapped(cell.date)
      }
    }
  }
}

