//
//  WeekBarConstants.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/19/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

struct WeekBarConstants {

  static let selectedBackgrouondColor = UIColor.black
  
  static let unselectedBackgrouondColor = UIColor.clear
  
  static let weekendTextColor = UIColor.lightGray
  
  static let selectedTextColor = UIColor.white
  
  static let unSelectedTextColor = UIColor.black
  

}
