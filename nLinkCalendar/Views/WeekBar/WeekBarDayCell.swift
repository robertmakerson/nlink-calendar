//
//  WeekBarDayCell.swift
//  nLinkCalendar
//
//  Created by Julian Hays on 2/27/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import Foundation
import UIKit
import RxCocoa

class WeekBarDayCell : ViewBase {
  
  var dayName = UILabel()
  
  var dayNumber = UILabel()
  
  var date = Date.today
  
  var weekend = false {
    didSet {
      if weekend {
        dayNumber.backgroundColor = WeekBarConstants.weekendTextColor
        dayName.textColor = WeekBarConstants.weekendTextColor
      } else {
        dayNumber.backgroundColor = WeekBarConstants.unselectedBackgrouondColor
        dayNumber.textColor = WeekBarConstants.unSelectedTextColor
      }
    }

  }
  
  var selected = false {
    didSet {
      if selected {
        dayNumber.backgroundColor = WeekBarConstants.selectedBackgrouondColor
        dayNumber.textColor = WeekBarConstants.selectedTextColor
      } else {
        dayNumber.backgroundColor = WeekBarConstants.unselectedBackgrouondColor
        dayNumber.textColor = weekend ? WeekBarConstants.weekendTextColor : WeekBarConstants.unSelectedTextColor
      }
    }
  }
  
  var tapped: (WeekBarDayCell) -> Void = { _ in }

  override func setup() {
    addSubview(dayName)
    addSubview(dayNumber)
    
    if UIDevice.current.userInterfaceIdiom == .pad {
      dayName.font = UIFont.regular(size: 16)
      dayName.textAlignment = .right
    } else {
      dayName.font = UIFont.regular(size: 11)
      dayName.textAlignment = .center
    }
    dayNumber.font = UIFont.regular(size: 16)
    dayNumber.layer.masksToBounds = true
    dayNumber.layer.cornerRadius = 12
    dayNumber.textAlignment = .center
    addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped(_:))))
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if UIDevice.current.userInterfaceIdiom == .pad {
      let gap : CGFloat = 6
      let halfWidth = (bounds.width - gap) / 2
      dayName.frame = CGRect(x: 0, y: 0, width: halfWidth, height: bounds.height)
      dayNumber.frame = CGRect(x: bounds.width - halfWidth, y: (bounds.height - 24)/2, width: 24, height: 24)
    } else {
      dayName.frame = CGRect(x: 0, y: 2, width: bounds.width, height: 16)
      dayNumber.frame = CGRect(x: (bounds.width - 24) / 2 , y: 20, width: 24, height: 24)
    }
  }
  
  @objc func tapped(_ button: UIButton) {
    tapped(self)
  }
}
