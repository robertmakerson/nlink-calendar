//
//  WeekAllDayEventContainer.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/29/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class WeekAllDayEventContainer: ViewBase {
  
  let allDayLabel = UILabel()
  
  var dividers = [UIView]()
  
  var dayCells = [DayCell]()
  
  var height : CGFloat {
    var height : CGFloat = 0
    for cell in dayCells {
      height = max(cell.height, height)
    }
    return height
  }
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      for cell in dayCells {
        cell.onSelect = onSelect
      }
    }
  }
  
  override func setup() {
    super.setup()
    clipsToBounds = true
    backgroundColor = DayViewConstants.allDayHeaderBackground
    for _ in 0..<7 {
      let divider = UIView()
      divider.backgroundColor = DayViewConstants.dividerColor - UIColor.gray(30)
      addSubview(divider)
      dividers.append(divider)
      
      let cell = DayCell()
      addSubview(cell)
      cell.onSelect = onSelect
      dayCells.append(cell)
    }
    
    allDayLabel.font = DayViewConstants.hourMarkerLabelFont
    allDayLabel.textAlignment = .right
    allDayLabel.textColor = DayViewConstants.allDayHeaderLabelColor
    allDayLabel.text = "All Day"
    addSubview(allDayLabel) 
  }
  
  func display(events: [CalendarEvent], at index: Int) {
    dayCells[index].display(events: events)
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    allDayLabel.frame = CGRect(x: 0, y: 0, width: DayViewConstants.timeLabelWidth, height: height)
    let dayWidth = (bounds.width - DayViewConstants.eventXOffset)  / CGFloat(dividers.count > 0 ? dividers.count :  1)
    for (idx, divider) in dividers.enumerated() {
      divider.frame = CGRect(x: DayViewConstants.eventXOffset + dayWidth * CGFloat(idx), y: 0, width: 1, height: height)
    }
    for (idx, cell) in dayCells.enumerated() {
      cell.frame = CGRect(x: DayViewConstants.eventXOffset + dayWidth * CGFloat(idx), y: 0, width: dayWidth, height: height)
    }
  }
  
  class DayCell : CalendarView {
    
    var eventCells = [UIView]()
    
    var height : CGFloat = 0
    
    var onSelect : (CalendarEvent) -> Void = { event in }
    
    override func setup() {
      super.setup()
    }
    
    func display(events: [CalendarEvent]) {
      for cell in eventCells {
        cell.removeFromSuperview()
      }
      
      for event in events {
        if let cell = context.delegate?.cell(for: event) {
          eventCells.append(cell)
          let tapRecog = UITapGestureRecognizer()
          cell.addGestureRecognizer(tapRecog)
          tapRecog.rx.event.bind(onNext: { [unowned self] recorgnizer in
            self.onSelect(event)
          }).disposed(by: disposeBag)
          addSubview(cell)
        }
      }
      height = CGFloat(events.count) * DayViewConstants.allDayRowHeight + 2
      layoutSubviews()
    }
    
    override func layoutSubviews() {
      super.layoutSubviews()
      for (idx, cell) in eventCells.enumerated()  {
        cell.frame = CGRect(x: 1, y: CGFloat(idx) * DayViewConstants.allDayRowHeight + 2, width: bounds.width - 4, height: DayViewConstants.allDayRowHeight - 2)
      }
    }
  }
  
}


