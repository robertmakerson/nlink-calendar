//
//  WeekBackground.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class WeekBackground: DayBackground {

  var dividers = [UIView]()
  
  override func setup() {
    super.setup()    
    for _ in 0..<7 {
      let divider = UIView()
      divider.backgroundColor = DayViewConstants.dividerColor
      addSubview(divider)
      dividers.append(divider)
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    let dayWidth = (bounds.width - DayViewConstants.eventXOffset)  / CGFloat(dividers.count > 0 ? dividers.count :  1)
    for (idx, divider) in dividers.enumerated() {
      divider.frame = CGRect(x: DayViewConstants.eventXOffset + dayWidth * CGFloat(idx), y: 0, width: 1, height: bounds.height)
    }
  }
}
