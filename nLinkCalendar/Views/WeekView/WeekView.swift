//
//  WeekView.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class WeekView: PrimaryCalendarView {

  var weekContainer = CarouselView()
  
  override func setup() {
    addSubview(weekContainer)
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    weekContainer.frame = bounds
    layoutDetails()
  }

  func configure() {
    weekContainer.delegate = self
  }
  
  override func onChange(to day: Date) {
    let newPage = day.startOfWeek.weeks(from: Date.today.startOfWeek)
    weekContainer.moveTo(to: newPage)
  }
  
  override func onGroupsChange() {
    weekContainer.reload()
  }
  
}

extension WeekView: CarouselViewDelegate {
  
  func pageView(for page: Int) -> UIView? {
    let sow = Date.today.startOfWeek
    let day = sow.add(page.weeks)
    let page = WeekViewPage()
    page.onSelect = displayDetails
    page.startOfWeek = day
    page.displayEvents()
    return page
  }
  
  func moved(to page: Int) {
    let currentDay = context.currentDay.value
    let currentOffset = currentDay.startOfWeek.weeks(from: Date.today.startOfWeek)
    let pageChange = page - currentOffset
    change(day: currentDay.add(pageChange.weeks))
  }
  
}
