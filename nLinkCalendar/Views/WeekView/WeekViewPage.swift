//
//  WeekViewPage.swift
//  nLinkCalendar
//
//  Created by John Bailey on 3/26/18.
//  Copyright © 2018 medtronic. All rights reserved.
//

import UIKit

class WeekViewPage: CalendarView {

  let header = WeekBarCellContainer()
  
  let allDayContainer = WeekAllDayEventContainer()
  
  let scrollView = UIScrollView()
  
  let background = WeekBackground()
  
  var dayContainers = [DayViewEventContainer]()
  
  var startOfWeek = Date.today {
    didSet {
      header.selectedDate = startOfWeek
    }
  }
  
  var onSelect : (CalendarEvent) -> Void = { event in } {
    didSet {
      for container in dayContainers {
        container.onSelect = onSelect
      }
      allDayContainer.onSelect = onSelect
    }
  }
  
  override func setup() {
    header.cellOffset = DayViewConstants.eventXOffset
    header.cellsSelectable = false
    addSubview(header)
    addSubview(allDayContainer)
    addSubview(scrollView)
    scrollView.addSubview(background)
    for _ in 0..<7 {
      let view = DayViewEventContainer()
      view.onSelect = onSelect
      dayContainers.append(view)
      scrollView.addSubview(view)
    }
    super.setup()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    header.frame = CGRect(x: 0, y: 0, width: bounds.width , height: 50)
    allDayContainer.frame = CGRect(x: 0, y: header.frame.maxY, width: bounds.width, height: allDayContainer.height)
    scrollView.frame = CGRect(x: 0, y: allDayContainer.frame.maxY, width: bounds.width, height: bounds.height - allDayContainer.frame.maxY)
    background.frame = CGRect(x: 0, y: 0, width: bounds.width, height: DayViewConstants.pixelPerDay + 28)
    let dayWidth = (bounds.width - DayViewConstants.eventXOffset)  / CGFloat(dayContainers.count > 0 ? dayContainers.count :  1)
    for (idx, dayView) in dayContainers.enumerated() {
      dayView.frame = CGRect(x: DayViewConstants.eventXOffset + CGFloat(idx) * dayWidth, y: 8, width: dayWidth, height: DayViewConstants.pixelPerDay)
    }
    scrollView.contentSize = CGSize(width: bounds.width, height: DayViewConstants.pixelPerDay + 28)
    
    scrollView.contentOffset = CGPoint(x: 0, y: DayViewConstants.pixelPerHour * 7)
  }
  

  func displayEvents() {
    for (offset, dayContainer) in dayContainers.enumerated() {
      let day = startOfWeek.add(offset.days)
      if day.startOfDay == Date.today.startOfDay {
        header.selectedCell = offset
      }      
      context.dayEvents(for: day) { events in
        dayContainer.display(events: events.filter( { !$0.allDay } ))
        self.allDayContainer.display(events: events.filter( { $0.allDay } ), at: offset)
        self.layoutSubviews()
      }
    }
    
  }
  
}
